# API Design Guidelines
This project contains documents that define the Conexxus/IFSF API Design Guidelines.  

* If the project is within the *Work in Progress* group, it contains Microsoft word documents, and the changes for any given development branch will be presented in "track changes" mode.
* If the project is within the *Public Standards* group. it contains the pdf versions of the documents.

## Guides in this Project
The guides fall into the following categories:

### Design Rules 
1. **[Open Retailing API Design Rules for JSON](Open%20Retailing%20API%20Design%20Rules%20for%20JSON.docx)**
   
   This guide covers naming of properties, use of booleans, and other basic design topics.

2. **[Open Retailing Design Rules for APIs OAS3.0](Open%20Retailing%20Design%20Rules%20for%20APIs%20OAS3.0.docx)**
   
   Topics in this guide describe how to define an API (using the API Definition File (ADF), written in YAML), expected directory layouts, and other important topics.

3. **[Open Retailing Implementation Guide - Security](Open%20Retailing%20API%20Implementation%20Guide%20-%20Security.docx)**
   
   This guide breaks out security considerations into a separate document.  Since ideas about security tend to change relatively rapidly, having this guide in a separate document makes the rest of the documents more stable.

4. **[Open Retailing Implementation Guide - Transport Alternatives](Open%20Retailing%20API%20Implementation%20Guide%20-%20Transport%20Alternatives.docx)**
   
   Transport alternatives include web sockets, native (TCP/IP) sockets, MQTT, AMQP, etc.  This document explains the current design choices for the API work and how that might change in the future, depending on changes to the cloud coding conventional wisdom.


